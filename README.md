# Cloud Seed :: Meta

The meta project for Cloud Seed - a place for drafting plans, directions, documentations etc.

## Useful Links

- [hello.cloudseed.app](https://hello.cloudseed.app)
- [Documentation](https://docs.gitlab.com/ee/cloud_seed/index.html)
- [Board](https://gitlab.com/groups/gitlab-org/incubation-engineering/five-minute-production/-/boards?group_by=epic)
- [Weekly's](https://www.youtube.com/playlist?list=PL05JrBw4t0Krf0LZbfg80yo08DW1c3C36)
- [Handbook](https://about.gitlab.com/handbook/engineering/incubation/cloud-seed/)
